<?php
	require_once("colors.php"); //pallet and size data
	require_once("gradient-fill.php"); //needed to make the gradients

	//variables 
	$pallet = $_POST['pallet'];
	$size = $_POST['size'];
	$font = $_POST['font'];
	$sources = $_POST['sources'];
	$sources = str_replace("\'", "'", $sources);
	$sources = str_replace('\"', '"', $sources);
	
	switch($size){
		case "small": $width = $small;
						break;
		case "medium": $width = $medium;
						break;
		case "large": $width = $large;
						break;
	}

	foreach($pallets_html as $key => $html){
		if($html[0] == $pallet){
			$bgcolor_reference = $pallets_text[$key][0];
			$main_header_color_ref = $pallets_text[$key][1];
			$sub_header_color_ref = $pallets_text[$key][2];
			$body_text_color_ref = $pallets_text[$key][3];
		}
	}
	
	foreach($fonts as $current){
		if($current[2] == $font)
			$font_path = $current[0];
	}
	
	if($font_path == null)
		$font_path = "../fonts/arial.ttf";
	if($bgcolor_reference == null){
		$bgcolor_reference = $black_on_white_text[0];
		$main_header_color_ref = $black_on_white_text[1];
		$sub_header_color_ref = $black_on_white_text[2];
		$body_text_color_ref = $black_on_white_text[3];
	}
	if($width == null || $width == 'n/a')
		$width = "800";

	$bgcolor_referencergb = hex2rgb($bgcolor_reference);
	$main_header_color_ref_rgb = hex2rgb($main_header_color_ref);
	$sub_header_color_ref_rgb = hex2rgb($sub_header_color_ref);
	$body_text_color_ref_rgb = hex2rgb($body_text_color_ref);


	if($bgcolor_referencergb[0] + 50 > 255)
		$bgcolor_referencergb[0] = 255;
	else
		$bgcolor_referencergb[0]=$bgcolor_referencergb[0]+50;

	if($bgcolor_referencergb[1] + 50 > 255)
		$bgcolor_referencergb[1] = 255;
	else
		$bgcolor_referencergb[1]=$bgcolor_referencergb[1]+50;
			

	if($bgcolor_referencergb[2] + 50 > 255)
		$bgcolor_referencergb[2] = 255;
	else
		$bgcolor_referencergb[2]=$bgcolor_referencergb[2]+50;
			
		
	$color2=rgb2hex($bgcolor_referencergb);
	$bgcolor_referencergb = hex2rgb($bgcolor_reference); 
	$background = imagecreate($width, 300);
	$backgroundcolor = imagecolorallocate($background, $bgcolor_referencergb[0], $bgcolor_referencergb[1], $bgcolor_referencergb[2]);
	$bgcolor = imagecolorallocate($background, $bgcolor_reference_explode[0], $bgcolor_reference_explode[1], $bgcolor_reference_explode[2]); 
	$main_header_color = imagecolorallocate($background, $main_header_color_ref_rgb[0], $main_header_color_ref_rgb[1], $main_header_color_ref_rgb[2]);
	$sub_header_color = imagecolorallocate($background, $sub_header_color_ref_rgb[0], $sub_header_color_ref_rgb[1], $sub_header_color_ref_rgb[2]);
	$body_text_color = imagecolorallocate($background, $body_text_color_ref_rgb[0], $body_text_color_ref_rgb[1], $body_text_color_ref_rgb[2]);

	
	
	
	$bbox = imagettfbbox(12, 0, $font_path, $sources);
	
	imageline($background, 30, 5, $width-30, 5, $sub_header_color);
	imagettftext($background, 16, 0, 35, 30, $sub_header_color, $font_path, "Sources:");
	imagettftext($background, 10, 0, 35, 55, $body_text_color, $font_path, $sources);
	$finalHeight = ((substr_count($sources, "\n")+2)*16)+40;
	$final=imagecreatetruecolor($width, $finalHeight);
	imagecopyresampled($final, $background, 0, 0, 0, 0, $width, 300, $width, 300);
	$x = 0;
	do{
		$x++;
	}while(file_exists($x.".png"));
	imagepng($final, $x . ".png");
	header("Location: ../index.php?preview=true&size=" . $_POST['size'] . "&pallet=" . $_POST['pallet'] . "&layout=" . $_POST['layout'] . "&font=" . $_POST['font']);
	$output = array(
		json_encode($pallets_html), json_encode($pallets_text), json_encode($bgcolor_referencergb));
	$f = fopen("test.json", "w"); 
	fwrite($f, json_encode($output)); 
	fclose($f);  
?>	
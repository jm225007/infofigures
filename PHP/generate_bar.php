<?php
	require_once("colors.php"); //pallet and size data
	require_once("gradient-fill.php"); //needed to make the gradients

	
	function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
	{
		/* this way it works well only for orthogonal lines
		imagesetthickness($image, $thick);
		return imageline($image, $x1, $y1, $x2, $y2, $color);
		*/
		if ($thick == 1) {
			return imageline($image, $x1, $y1, $x2, $y2, $color);
		}
		$t = $thick / 2 - 0.5;
		if ($x1 == $x2 || $y1 == $y2) {
			return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
		}
		$k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
		$a = $t / sqrt(1 + pow($k, 2));
		$points = array(
			round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
			round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
			round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
			round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
		);
		imagefilledpolygon($image, $points, 4, $color);
		return imagepolygon($image, $points, 4, $color);
	}
	
	//variables 
	$pallet = $_POST['pallet'];
	$size = $_POST['size'];
	$font = $_POST['font'];
	$title = $_POST['title'];
	$description = $_POST['description'];
	
	
	
	for($x = 1; $x < 15; $x++){
		if($_POST['Title'.$x] != null || $_POST['Data'.$x] != null) {
			$data[$x-1]=$_POST['Data'.$x];
			$title[$x-1]=$_POST['Title'.$x];
		}
	}
	
	
	switch($size){
		case "small": $width = $small;
						break;
		case "medium": $width = $medium;
						break;
		case "large": $width = $large;
						break;
	}

	foreach($pallets_html as $key => $html){
		if($html[0] == $pallet){
			$bgcolor_reference = $pallets_text[$key][0];
			$main_header_color_ref = $pallets_text[$key][1];
			$sub_header_color_ref = $pallets_text[$key][2];
			$body_text_color_ref = $pallets_text[$key][3];
		}
	}
	
	foreach($pallets_html as $key => $html){
		if($html[0] == $pallet){
			$dark_ref = $pallets_graphics[$key][0];
			$midDark_ref = $pallets_graphics[$key][1];
			$midLight_ref = $pallets_graphics[$key][2];
			$light_ref = $pallets_graphics[$key][3];
		}
	}
	
	foreach($fonts as $current){
		if($current[2] == $font)
			$font_path = $current[0];
	}
	
	
	if($font_path == null)
		$font_path = "../fonts/arial.ttf";
	if($bgcolor_reference == null){
		$bgcolor_reference = $black_on_white_text[0];
		$main_header_color_ref = $black_on_white_text[1];
		$sub_header_color_ref = $black_on_white_text[2];
		$body_text_color_ref = $black_on_white_text[3];
	}
	if($width == null || $width == 'n/a')
		$width = "800";

		
	if($dark_ref == null){
		$dark_ref = $black_on_white_text[0];
		$midDark_ref = $black_on_white_text[1];
		$midLight_ref = $black_on_white_text[2];
		$light_ref = $black_on_white_text[3];
	}
	
	$main_header_color_ref_rgb = hex2rgb($main_header_color_ref);
	$sub_header_color_ref_rgb = hex2rgb($sub_header_color_ref);
	$body_text_color_ref_rgb = hex2rgb($body_text_color_ref);


	$light_rgb = hex2rgb($light_ref);
	$midLight_rgb = hex2rgb($midLight_ref);
	$midDark_rgb = hex2rgb($midDark_ref);
	$dark_rgb = hex2rgb($dark_ref);

	if($bgcolor_referencergb[0] + 50 > 255)
		$bgcolor_referencergb[0] = 255;
	else
		$bgcolor_referencergb[0]=$bgcolor_referencergb[0]+50;

	if($bgcolor_referencergb[1] + 50 > 255)
		$bgcolor_referencergb[1] = 255;
	else
		$bgcolor_referencergb[1]=$bgcolor_referencergb[1]+50;
			

	if($bgcolor_referencergb[2] + 50 > 255)
		$bgcolor_referencergb[2] = 255;
	else
		$bgcolor_referencergb[2]=$bgcolor_referencergb[2]+50;
			
		
	$color2=rgb2hex($bgcolor_referencergb);
	$bgcolor_referencergb = hex2rgb($bgcolor_reference); 
	$background = imagecreate($width, 350);
	$backgroundcolor = imagecolorallocate($background, $bgcolor_referencergb[0], $bgcolor_referencergb[1], $bgcolor_referencergb[2]);
	$bgcolor = imagecolorallocate($background, $bgcolor_reference_explode[0], $bgcolor_reference_explode[1], $bgcolor_reference_explode[2]); 
	$main_header_color = imagecolorallocate($background, $main_header_color_ref_rgb[0], $main_header_color_ref_rgb[1], $main_header_color_ref_rgb[2]);
	$sub_header_color = imagecolorallocate($background, $sub_header_color_ref_rgb[0], $sub_header_color_ref_rgb[1], $sub_header_color_ref_rgb[2]);
	$body_text_color = imagecolorallocate($background, $body_text_color_ref_rgb[0], $body_text_color_ref_rgb[1], $body_text_color_ref_rgb[2]);
	
	$dark = imagecolorallocate($background, $dark_rgb[0], $dark_rgb[1], $dark_rgb[2]);
	$dark = imagecolorallocate($background, $dark_rgb[0], $dark_rgb[1], $dark_rgb[2]);
	$light = imagecolorallocate($background, $light_rgb[0], $light_rgb[1], $light_rgb[2]);
	$midDark = imagecolorallocate($background, $midDark_rgb[0], $midDark_rgb[1], $midDark_rgb[2]);
	$midLight = imagecolorallocate($background, $midLight_rgb[0], $midLight_rgb[1], $midLight_rgb[2]);

	
	imageline($background, 30, 10, $width-30, 10, $body_text_color);
	imagelinethick($background, 30, 70, 30, 310, $body_text_color, 2);
	imagelinethick($background, 30, 310, $width/2, 310, $body_text_color, 2);
	
	header('content-type: image/png');
	imagepng($background);
	imagedestroy($background);
	
	/*
	
	$x = 0;
	do{
		$x++;
	}while(file_exists($x.".png"));
	imagepng($final, $x . ".png");
	header("Location: ../index.php?preview=true&size=" . $_POST['size'] . "&pallet=" . $_POST['pallet'] . "&layout=" . $_POST['layout'] . "&font=" . $_POST['font']);
	*/
	
	?>	